require 'omniauth-oauth2'
require 'multi_json'


module OmniAuth

  module Strategies
    #
    # Usage:
    #    use OmniAuth::Strategies::Nanoleaf, 'consumerkey', 'consumersecret', :scope => 'read write', :display => 'plain'
    #
    class Nanoleaf < OmniAuth::Strategies::OAuth2


      DEFAULT_SCOPE = 'identify'

      BASE_URL = ENV['RAILS_ENV'] == 'production' ? ENV['AUTH_BASE_URL'] : 'https://my-test.nanoleaf.me'

      puts "BASE URL IS =>#{BASE_URL}"

      option :name, "nanoleaf"
      option :fields, [:name, :email]

      option :client_options, {
        :authorize_url => "#{BASE_URL}/oauth2/authorize",
        :token_url => "#{BASE_URL}/oauth2/token",
        :site => BASE_URL
      }


      option :authorize_options, [:scope]

      uid { raw_info['uid'] }

      info do
        {
          :id            => raw_info['info']['id'],
          :email         => raw_info['info']['email'],
          :access_token  => raw_info['info']['access_token'],
          :refresh_token => raw_info['info']['refresh_token']
        }
      end

      extra do
        {
          'raw_info' => raw_info
        }
      end

      def raw_info
        @raw_info = access_token.get('api/v1/users/info').parsed

      end



      # Over-ride callback_url definition to maintain
      # compatability with omniauth-oauth2 >= 1.4.0
      #
      # See: https://github.com/intridea/omniauth-oauth2/issues/81
      def callback_url
        #full host comes back to the current url that made the initial request
        full_host + script_name + callback_path
      end

      # Hook useful for appending parameters into the auth url before sending
      # to provider.
      def request_phase
        super
      end

      # Hook used after response with code from provider. Used to prep token
      # request from provider.
      def callback_phase

        super


      end



      def authorize_params
        super.tap do |params|
          options[:authorize_options].each do |option|
            params[option] = request.params[option.to_s] if request.params[option.to_s]
          end

          params[:redirect_uri] = options[:redirect_uri] unless options[:redirect_uri].nil?
          params[:scope] ||= DEFAULT_SCOPE
        end
      end

    end
  end
end
