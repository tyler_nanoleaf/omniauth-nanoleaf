# Nanoleaf's Omniauth strategy

This is the official Ruby OmniAuth strategy for authenticating to [Nanoleaf](https://my.nanoleaf.me).

Before you can start developing your API client for Nanoleaf, you need to create an client the nanoleaf cloud client services, and get the client id and secret

## Installation

Add the strategy to your `Gemfile`:

```ruby
gem 'omniauth-nanoleaf'
```

And bundle.

## Usage

You can integrate the strategy into your middleware in a `config.ru`:


If you're using Rails, you'll want to add to the middleware stack:

```ruby
Rails.application.config.middleware.use OmniAuth::Builder do
  provider :nanoleaf, SETTINGS['CLIENT_ID'], SETTINGS['CLIENT_SECRET'], scope: "read write"
end
```

- The scope needs to be separated by space and not comma: "public redeploy" instead of "public,redeploy" !

For additional information, refer to the [OmniAuth wiki](https://github.com/intridea/omniauth/wiki).